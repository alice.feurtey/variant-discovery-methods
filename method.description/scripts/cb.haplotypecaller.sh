for f1 in ../cb25.map/*rg.bam
do
cd ../Programs/gatk-4.0.6.0/
./gatk HaplotypeCaller -R ../../chr1.fa -I ${f1} -O ${f1}.g.vcf.gz -ERC GVCF -ploidy 1
done