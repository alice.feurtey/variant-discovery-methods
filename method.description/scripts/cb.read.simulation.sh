for f1 in ../cb.ref/*fa
do
art_illumina -ss HS10 -sam -i ${f1} -p -l 100 -f 20 -m 200 -s 10 -o ${f1}.100
art_illumina -ss HS10 -sam -i ${f1} -p -l 25 -f 20 -m 200 -s 10 -o ${f1}.25
done