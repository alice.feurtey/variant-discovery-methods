from collections import defaultdict
import random


#Here are the inputs the script needs from you
name_tree_table = "tree_as_tablezt"
directory_vcf = ""
name_vcf_indels = "refindel.refseq2simseq.INDEL.vcf"
subsample_nb = 1000 #This will sample from the input vcf. If you do not want the number reduced, just set this to the number of indels in the vcf.

full_path_vcf_indels = "/".join((directory_vcf, name_vcf_indels))


#Read vcf file
header = []
input_variants = []
with open(full_path_vcf_indels) as vcf_indels_input :
    for l in vcf_indels_input : 
        if l.startswith("#"): 
            header.append(l)
        else :
            input_variants.append(l)
nb_variants = len(input_variants)
print(len(input_variants))
input_variants = random.sample(input_variants, subsample_nb)
print(len(input_variants))

#Read tree, get branch lengths and a list of leaves from each branch
leaves = set()
branch_lengths = {}
samples_per_branch = {}
with open(name_tree_table) as tree_table_input :
    for l in tree_table_input :
        lsp = l.strip().split("\t")
        branch_lengths[lsp[0]] = float(lsp[1])
        samples_per_branch[lsp[0]] = lsp[2:]
        leaves.update(lsp[2:])
        
#In order to weight the branches by their length, I create a list from which 
#I will randomly draw
list_to_draw = []
for branch in branch_lengths :
    count = int(round(branch_lengths[branch]*1000, 0))
    list_to_draw.extend([branch]*count)
    
#For each variant, choose a branch randomly (weight it by the branch length?)
#Then add the variant in the file of the corresponding sample
indels_per_leaf = defaultdict(lambda: [])
for indel in input_variants :
    selected_branch = random.choice(list_to_draw)  
    for leaf in samples_per_branch[selected_branch] :
        indels_per_leaf[leaf].append(indel)
        
#Write all files
for sample in leaves :
    name_vcf_output = ".".join(("Per_sample_indels", sample, "vcf"))
    full_path_vcf_output = "/".join((directory_vcf, name_vcf_output))
    with open(full_path_vcf_output, "w") as sample_vcf :
        sample_vcf.write("".join(header))
        sample_vcf.write("".join(indels_per_leaf[sample]))
    