for f1 in ../cb25.map/*rmdup.bam
do
 cd /Programs/broadinstitute-picard-f34d34f/picard/
  java -jar build/libs/picard.jar AddOrReplaceReadGroups I=${f1} O=${f1}.rg.bam RGID=${f1} RGLB=lib1 RGPL=illumina RGPU=unit1 RGSM=${f1}
done