#For Cercospora beticola
#Prep for simulation
	#to remove ambiguous characters
		sed -i "s/N//g" chr1.fa

	#alf fasta2darwin for simulation
		fasta2darwin ../../chr1.fa -o ../../chr1.db -d

#Population Simulation
	#ALF 
		./../Programs/alfsim/ALF_standalone/bin/alfsim -o cb.data cb.popsim.drw
	#MafFilter to confirm Watterson's theta
		#cb.theta.bpp parameter file
	#Indel Simulation
		#SimuG to simulate 1000 random indels in the reference chromosome
			perl ../Programs/simug-master/simuG.pl -refseq chr1.fa -indel_count 1000
		#Newick tree from ALF used to make a table of branches to show phylogenetic relationships to one another
		#Above table used with Get_vcf_per_sample.py to assign indels according to the tree
		#SimuG to add indels in each sequence
			perl ../Programs/simug-master/simuG.pl -refseq #SE0XX -indel_vcf #SE0XX -p SE0XX
	#Accessory region introduction
		#Produce a random string of 290 kb, insert manually at position 3400000 in sequences SE001, SE009, SE011, SE012, SE014
		#Produce 5 random strings of 58 kb strings, insert manually at positions 1110000, 2270000, 3000000, 4210000, 5100000 in SE002
	#Read simulation
		#cb.read.simulation.sh
	#Make all directories needed and copy reads to folders
		mkdir cb25.map cb25.wga cb100.map cb100.wga
		cp *.25 cb25.map
		cp *.25 cb25.wga
		cp *.100 cb100.map
		cp *.100 cb100.wga
		
#Read mapping pipeline, show for 25X, substitute in 100X for the equivalent analysis in the 100X folder. Edit scripts accordingly
	#BWA
		./cb.bwa.sh
	#SAMtools processing
		./cb.samtools1.sh
	#Read Group
		./cb.readgroup.sh
	#Indexing
		./cb.indexing.sh
	#GATK
		#HaplotypeCaller
			./cb.haplotypecaller.sh
		#CombineGVCFs
			./gatk CombineGVCFs -R chr1.fa \
			#Here list all g.vcfs produced by HaplotypeCaller \
			-O cb25.map/cb25.combined.g.vcf.gz
		#GenotypeGVCFs
			./gatk GenotypeGVCFs -R chr1.fa -ploidy 1 -V cb25.map/cb25.combined.g.vcf.gz -O cb25.map/cb25.map.vcf
	#VCF filtering
		vcftools --vcf cb25.map/cb25.map.vcf --max-missing 1 --remove-indels --recode --out cb25.map.nomissing.noindel
		vcf-to-tab cb25.map.nomissing.noindel.recode.vcf cb25.map.tab
	#Organisation			
		mkdir tabs
		cp cb25.map/cb25.map.tab tabs/

#dnWGA pipeline, again only 25X
	#Assembly
		./cb.spades.sh
	#Here manually copy out and rename all the final scaffold level assemblies into /cb25.map
	#LASTZ. Here see https://www.bx.psu.edu/miller_lab/dist/tba_howto.pdf for full description
		#Formating header of assemblies, rename file accordingly
			python formatSeq.py
		#Preparing alignments. This is best run within the folder you want to work in. Also change number of cores according to your system
			all_bz - "#newick tree from ALF output" >& cb25.map_bz.log
			parallel -j 30 < cb25.map_bz.log
		#Rename output maf cb25.wga.maf
	#MafFilter for variant calling
		#maffilter param=cb25.wga.bpp
	#VCF filtering
		vcftools --vcf cb25.map/cb25.wga.vcf --max-missing 1 --remove-indels --recode --out cb25.wga.nomissing.noindel
		vcf-to-tab cb25.wga.nomissing.noindel.recode.vcf cb25.wga.tab
	#same organisation as before
	
#These VCFs are then used by plotting.in.R to make the figures